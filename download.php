<?php
    session_start();
    if (isset($_GET["web-name"]) && isset($_GET["user-id"]) && isset($_SESSION["current_user_id"])) {
        if ($_SESSION["current_user_id"] == $_GET["user-id"]) {
            shell_exec("zip ".$_GET["web-name"]." ".$_GET["web-name"]);
            header("Location: ".$_GET["web-name"].".zip");
        }
    } else {
        header("Location: panel.php");
    }
?>