#!/bin/bash

mkdir $1
    echo $2 > $1/index.php
mkdir $1/css
mkdir $1/css/user
    > $1/css/user/estilo.css
mkdir $1/css/admin
    > $1/css/admin/estilo.css
mkdir $1/img
mkdir $1/img/avatars
mkdir $1/img/buttons
mkdir $1/img/products
mkdir $1/img/pets
mkdir $1/js
mkdir $1/js/validations
    > $1/js/validations/login.js
    > $1/js/validations/register.js
mkdir $1/js/effects
    > $1/js/effects/panels.js
mkdir $1/tpl
    > $1/tpl/main.tpl
    > $1/tpl/login.tpl
    > $1/tpl/register.tpl
    > $1/tpl/panel.tpl
    > $1/tpl/profile.tpl
    > $1/tpl/crud.tpl
mkdir $1/php
    > $1/php/create.php
    > $1/php/read.php
    > $1/php/update.php
    > $1/php/delete.php
    > $1/php/dbconnect.php