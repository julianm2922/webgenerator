<?php 
    session_start();
    $notice = "";
    $alert = "";
    if (!isset($_SESSION["current_user_email"]) || !isset($_SESSION["current_user_id"])) {
        header("Location: login.php");
    }
    $hostname = "localhost";
    $user = "adm_webgenerator";
    $pass = "webgenerator2020";
    $database = "webgenerator";

    $connection = mysqli_connect($hostname, $user, $pass, $database);
    if (isset($_POST["submit"])) {
        if ($_POST["web-name"] == "") {
            $alert = "Debe introducir un nombre para la web.";
        } else {
            $new_web = $_SESSION["current_user_id"].$_POST["web-name"];
            $response = mysqli_query($connection, "SELECT * FROM webs");
            if (mysqli_num_rows($response) > 0) {
                while ($row = mysqli_fetch_array($response, MYSQLI_ASSOC)) {
                    if ($row["dominio"] == $new_web) {
                        $alert = "Ya existe una web con ese nombre.";
                        break;
                    }
                }
            }
            if ($alert == "") {
                $connection->query('INSERT INTO `webs` VALUES (NULL, "'.$_SESSION["current_user_id"].'", "'.$new_web.'", CURRENT_TIMESTAMP)');
                $var = shell_exec("./wix.sh ".$new_web." ".$new_web);
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Panel de <?php echo $_SESSION["current_user_email"]; ?></title>
</head>
<body>
    <div class="window panel-page">
        <div class="nav">
            <h1 class="window-title fix">Bienvenido a tu panel</h1>
            <a href="logout.php" class="logout">Cerrar sesión de <?php echo $_SESSION["current_user_id"]; ?></a>
        </div>

        <h2>Generar web de:</h2>
        <form action="" method="POST">
            <input type="text" name="web-name" id="web-name" placeholder="Nombre de la web" class="ctrl-in">
            <p class="register-alert"><?php echo $alert; ?></p>
            <button type="submit" name="submit" class="ctrl-btn">Crear web</button>
            <p class="weblist-notice"><?php echo $notice; ?></p>
            <div class="weblist">
            <?php
                $res = "";
                if ($_SESSION["current_user_email"] == "admin@server.com") {
                    $res = mysqli_query($connection, "SELECT * FROM `webs`");
                } else {
                    $res = mysqli_query($connection, "SELECT * FROM `webs` WHERE `idUsuario`='".$_SESSION["current_user_id"]."'");
                }
                if (mysqli_num_rows($res) > 0) {
                    $notice = "Mostrando ".mysqli_num_rows($res)." páginas web creadas por usted.";
                    while ($item = mysqli_fetch_array($res)) {
                        echo '<div class="weblist-item"><a href="'.$item["dominio"].'/index.php">'.$item["dominio"].'</a> <a href="download.php?web-name='.$item["dominio"].'&user-id='.$_SESSION["current_user_id"].'">Descargar</a> <a href="delete.php?web-name='.$item["dominio"].'&user-id='.$_SESSION["current_user_id"].'">Eliminar</a></div>';
                    }
                } else {
                    $notice = "No ha creado ninguna página web aún.";
                }
            ?>
            </div>
        </form>
    </div>
</body>
</html>