<?php 
    session_start();
    $login_alert = "";
    if (isset($_POST["submit"])) {
        if ($_POST["email"] == "" || $_POST["passwd"] == "") {                              // campos vacíos
            $login_alert = "Debe llenar los dos campos.";
        } else {
            $hostname = "localhost";
            $user = "adm_webgenerator";
            $pass = "webgenerator2020";
            $database = "webgenerator";

            $connection = mysqli_connect($hostname, $user, $pass, $database);                   // conectándose a la base de datos
            if ($connection->connect_error) {
                die("Error al conectarse a la base de datos: ".$connection->conenction_error);  // no se pudo, tira error
            } else {
                $response = mysqli_query($connection, "SELECT * FROM `usuarios`");
                if (mysqli_num_rows($response) > 0) {
                    while ($row = mysqli_fetch_array($response, MYSQLI_ASSOC)) {
                        if ($row["email"] == $_POST["email"]) {
                            if ($row["password"] == $_POST["passwd"]) {
                                $_SESSION["current_user_email"] = $_POST["email"];
                                $_SESSION["current_user_id"] = mysqli_fetch_array(mysqli_query($connection, "SELECT `idUsuario` FROM `usuarios` WHERE `email`='".$_POST["email"]."'"), MYSQLI_ASSOC)["idUsuario"];
                                header("Location: panel.php");
                                break;
                            } else {
                                $login_alert = "Contraseña incorrecta.";
                            }
                        }
                    }
                    if ($login_alert == "") { 
                        $login_alert = "El usuario no está registrado."; 
                    }
                } else {
                    $login_alert = "No hay usuarios registrados.";
                }
            }
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>webgenerator Julián Miño</title>
</head>
<body>
    <div id="container">
        <div class="window">
            <h1 class="window-title">Iniciar sesión</h1>
            <form action="" method="POST">
                <input class="ctrl-in" type="email" name="email" id="email" placeholder="Correo electrónico">
                <input class="ctrl-in" type="password" name="passwd" id="passwd" placeholder="Contraseña">
                <p class="register-alert"><?php echo $login_alert ?></p>
                <button class="ctrl-btn" type="submit" name="submit">Iniciar sesión</button>
                <a class="ctrl-link" href="register.php">Registrarse</a>
            </form>
        </div>
    </div>
</body>
</html>