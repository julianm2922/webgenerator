<?php
    session_start();
    $register_alert = "";
    if (isset($_POST["submit"])) {
        if ($_POST["passwd"] == "" || $_POST["passwd-conf"] == "" || $_POST["email"] == "") {   // campos vacíos
            $register_alert = "Debe llenar todos los campos.";
        } else if ($_POST["passwd"] != $_POST["passwd-conf"]) {                                 // contraseñas no coinciden
            $register_alert = "Las contraseñas no coinciden.";
        } else {                                                                                // campos correctamente introducidos
            $hostname = "localhost";
            $user = "adm_webgenerator";
            $pass = "webgenerator2020";
            $database = "webgenerator";

            $connection = mysqli_connect($hostname, $user, $pass, $database);                   // conectándose a la base de datos
            if ($connection->connect_error) {
                die("Error al conectarse a la base de datos: ".$connection->conenction_error);  // no se pudo, tira error
            } else {
                $response = mysqli_query($connection, "SELECT * FROM `usuarios`");              // se pudo, obteniendo la tabla "usuarios"

                if (mysqli_num_rows($response) > 0) {
                    while ($row = mysqli_fetch_array($response, MYSQLI_ASSOC)) {                // si hay entradas en la tabla de usuarios,
                        if ($_POST["email"] == $row["email"]) {                                 // se busca una coincidencia entre alguno
                            $register_alert = "E-mail ya registrado.";                          // existente y el que se introdujo en el form.
                            break;
                        }
                    }
                }
                if ($register_alert == "") {                                                    // si el usuario no está registrado , se añade a la base de datos
                    $connection->query('INSERT INTO `usuarios` (`idUsuario`, `email`, `password`, `fechaRegistro`) VALUES (NULL, "'.$_POST["email"].'", "'.$_POST["passwd"].'", CURRENT_TIMESTAMP)');
                    $_SESSION["current_user_email"] = $_POST["email"];                          // se realiza el logueo
                    $_SESSION["current_user_id"] = mysqli_fetch_array(mysqli_query($connection, "SELECT `idUsuario` FROM `usuarios` WHERE `email`='".$_POST["email"]."'"), MYSQLI_ASSOC)["idUsuario"];
                    header("Location: panel.php");
                }
            }   
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Registrarte es simple.</title>
</head>
<body>
    <div id="container">
        <div class="window">
            <h1 class="window-title">Crear una cuenta</h1>
            <form action="" method="POST">
                <input class="ctrl-in" type="email" name="email" id="email" placeholder="Correo electrónico">
                <input class="ctrl-in" type="password" name="passwd" id="passwd" placeholder="Contraseña">
                <input class="ctrl-in" type="password" name="passwd-conf" id="passwd-conf" placeholder="Repita la contraseña">
                <p class="register-alert"><?php echo $register_alert ?></p>
                <button id="submit" class="ctrl-btn" type="submit" name="submit">Registrarme</button>
                <a class="ctrl-link" href="login.php">Ya tengo una cuenta</a>
            </form>
        </div>
    </div>
</body>
</html>